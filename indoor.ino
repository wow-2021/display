#ifndef BOARD_HAS_PSRAM
#error "Please enable PSRAM !!!"
#endif

#include <Arduino.h>
#include <TimeLib.h>
#include <esp_task_wdt.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "epd_driver.h"
#include "esp_adc_cal.h"
#include <Wire.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoHttpClient.h>
#include <Arduino_JSON.h>

#include "firasans.h"

#define BATT_PIN            36

uint8_t *framebuffer;
//int vref = 1100;

/****Debug from home**********************/
const char* ssid = "berryfamily"; 
const char* password = "A85888133-0";
const char* mqtt_server = "192.168.1.58";
const int mqtt_port = 2883;
const int http_port = 8080;
/******************************************/

/****Debug with 4G-M1**********************/
//const char* ssid = "xin-phone"; 
//const char* password = "31858877";
//const char* mqtt_server = "xins.dev";
//const int mqtt_port = 1883;
/******************************************/

String user_name = "vLm45dvykEUuxBZRC41c";
String clientID ="WoW indoor screen";

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTime = 0;
// Timer set to 10 minutes (600000)
//unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
unsigned long timerDelay = 5000;


WiFiClient espClient;
PubSubClient psClient(espClient);
HttpClient httpClient = HttpClient(espClient, mqtt_server, http_port);

String PROBE_1 = "d3e83ce0-26a3-11ec-b807-bdca9a00d16b";
String PROBE_2 = "10b8a040-5cd4-11ec-8619-d3619a138d77";

void initWiFi() {
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

    // Run in school
  WiFi.disconnect(true);  //disconnect form wifi to set new wifi connection
  delay(50);
  
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println(WiFi.localIP()); //print LAN IP
    printScreen(200, 20, 700, 50, "Connecting to WiFi ...");
    Serial.print(WiFi.status());
    Serial.println(WiFi.localIP()); //print LAN IP
    Serial.println(F("."));
    delay(1000);
  }
  Serial.println(WiFi.localIP());
  String info = "Connected to WiFi: "+ String(ssid);
  delay(1000);
  printScreen(200, 20, 700, 50, info);
  info = "IP: "+ IpAddress2String(WiFi.localIP()) +", RSSI: " + String(WiFi.RSSI());
  delay(1000);
  printScreen(200, 70, 700, 50, info);
  delay(1000);
}

void setup() {
    Serial.begin(115200);

    // Correct the ADC reference voltage
//    esp_adc_cal_characteristics_t adc_chars;
//    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars);
//    if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
//        Serial.printf("eFuse Vref:%u mV", adc_chars.vref);
//        vref = adc_chars.vref;
//    }

    epd_init();
//    framebuffer = (uint8_t *)ps_calloc(sizeof(uint8_t), EPD_WIDTH * EPD_HEIGHT / 2);
//    if (!framebuffer) {
//        Serial.println("alloc memory failed !!!");
//        while (1);
//    }
//    memset(framebuffer, 0xFF, EPD_WIDTH * EPD_HEIGHT / 2);
//
//    Rect_t area = {
//        .x = 230,
//        .y = 20,
//        .width = logo_width,
//        .height = logo_height,
//    };
//
    epd_poweron();
    epd_clear();
//    epd_draw_grayscale_image(area, (uint8_t *)logo_data);
    epd_poweroff();

    initWiFi();
    psClient.setServer(mqtt_server, mqtt_port);
}

void printScreen(int x, int y, int width, int height, String text){
  epd_poweron();
  
  Rect_t area = {
        .x = x,
        .y = y,
        .width = width,
        .height = height,
    };

    int cursor_x = x;
    int cursor_y = y + 40;
    epd_clear_area(area);
    writeln((GFXfont *)&FiraSans, (char *)text.c_str(), &cursor_x, &cursor_y, NULL);

    epd_poweroff();
}

void loop() {
  if (!psClient.connected()) {
    reconnect();
  }
  psClient.loop();

  String token = loginTB();
  
  refreshSensorStatus(token, PROBE_1, "temperature,humidity,light,soilmoisture,uv", 0);
  refreshSensorStatus(token, PROBE_2, "temperature,humidity,light,soilmoisture,uv", 480);
    
  delay(2*60*1000);
}

void refreshSensorStatus(String token, String devId, String ks, int leftOffset){
  String devName = getDevName(token, devId);
  Serial.print("Device: ");
  Serial.println(devName);
  printScreen(15+leftOffset, 150, 450, 50, devName);
  
  unsigned long ts = fetchLatestReading(token, devId, ks, leftOffset, 250);
  char buff[32];
  sprintf(buff, "%02d.%02d.%02d %02d:%02d:%02d", day(ts), month(ts), year(ts), hour(ts), minute(ts), second(ts));
  Serial.print("Last update: ");
  Serial.println(buff);
  printScreen(30+leftOffset, 200, 450, 50, buff);
}

void reconnect() {
  // Loop until we're reconnected
  while (!psClient.connected()) {
    // Attempt to connect
    if (psClient.connect(clientID.c_str(),user_name.c_str(),NULL)) {
      sendAttr();
    } else {
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// author apicquot from https://forum.arduino.cc/index.php?topic=228884.0
String IpAddress2String(const IPAddress& ipAddress)
{
    return String(ipAddress[0]) + String(".") +
           String(ipAddress[1]) + String(".") +
           String(ipAddress[2]) + String(".") +
           String(ipAddress[3]);
}

void sendAttr() {
  String msg_attribute ="{";  
  msg_attribute += "\"mac\":";
  msg_attribute += "\"" + String(WiFi.macAddress())+ "\"";
  msg_attribute += ",";
  msg_attribute += "\"ip\":";
  msg_attribute += "\"" + WiFi.localIP().toString() + "\"";
  msg_attribute += ",";
  msg_attribute += "\"rssi\":";
  msg_attribute += "\"" + String(WiFi.RSSI()) + "\"";
  msg_attribute += "}";
  psClient.publish("v1/devices/me/attributes" ,msg_attribute.c_str());
}

String loginTB(){
  String postData = "{\"username\":\"tenant@thingsboard.org\", \"password\":\"2B8TDrvw\"}";

  httpClient.beginRequest();
  httpClient.post("/api/auth/login");
  httpClient.sendHeader("Content-Type", "application/json");
  httpClient.sendHeader("Content-Length", postData.length());
  httpClient.sendHeader("Accept", "application/json");
  httpClient.beginBody();
  httpClient.print(postData);
  httpClient.endRequest();

  // read the status code and body of the response
  int statusCode = httpClient.responseStatusCode();
  String response = httpClient.responseBody();

  Serial.print("Login code: ");
  Serial.println(statusCode);

  httpClient.stop();

  JSONVar jsonObject = JSON.parse(response);
  // JSON.typeof(jsonVar) can be used to get the type of the var
  if (JSON.typeof(jsonObject) == "undefined") {
    Serial.println("Parsing input failed!");
    return "";
  }

  String token =  (const char*) jsonObject["token"];

  return token;
}

String getDevName(String token, String devId){
  String jwt_token = "Bearer " + token;

  httpClient.beginRequest();
  httpClient.get("/api/device/info/" + devId);
  httpClient.sendHeader("X-Authorization", jwt_token);
  httpClient.endRequest();

  // read the status code and body of the response
  int statusCode = httpClient.responseStatusCode();
  int bodyLen = httpClient.contentLength();
  String response = httpClient.responseBody();
  httpClient.stop();
  
  JSONVar jsonObject = JSON.parse(response);
  // JSON.typeof(jsonVar) can be used to get the type of the var
  if (JSON.typeof(jsonObject) == "undefined") {
    Serial.println("Parsing input failed!");
    return "";
  }

  return (const char*)jsonObject["name"];
}

unsigned long fetchLatestReading(String token, String devId, String ks, int offset, int top){
  String jwt_token = "Bearer " + token;

  httpClient.beginRequest();
  httpClient.get("/api/plugins/telemetry/DEVICE/"+devId+"/values/timeseries?keys="+ks);
//  httpClient.sendHeader("Content-Type", "application/json");
//  httpClient.sendHeader("Accept", "application/json;charset=UTF-8");
  httpClient.sendHeader("X-Authorization", jwt_token);
  httpClient.endRequest();

  // read the status code and body of the response
  int statusCode = httpClient.responseStatusCode();
  int bodyLen = httpClient.contentLength();
  String response = httpClient.responseBody();
  httpClient.stop();
  
//  Serial.print("Reading code: ");
//  Serial.println(statusCode);
//  Serial.print("Content length is: ");
//  Serial.println(bodyLen);
//  Serial.print("Content: ");
//  Serial.println(response);

  JSONVar jsonObject = JSON.parse(response);
  // JSON.typeof(jsonVar) can be used to get the type of the var
  if (JSON.typeof(jsonObject) == "undefined") {
    Serial.println("Parsing input failed!");
    return 0;
  }

  // myObject.keys() can be used to get an array of all the keys in the object
  JSONVar keys = jsonObject.keys();
  unsigned long ts;
  for (int i = 0; i < keys.length(); i++) {
    JSONVar value = jsonObject[keys[i]];
    printScreen(5+offset, top+i*50, 250, 50, (const char*)keys[i]);
    printScreen(255+offset, top+i*50, 50, 50, ": ");
    printScreen(305+offset, top+i*50, 150, 50, (const char*)value[0]["value"]);
    
    String jsonString = JSON.stringify(value[0]["ts"]);
    jsonString = jsonString.substring(0,jsonString.length()-3);
    ts = atol(jsonString.c_str());
  }
  
  return ts+(8*60*60); // Convert to Timezone +8
}
